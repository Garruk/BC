import svgwrite, random, math, string
from svgwrite import mixins, base

#Main function, creates and saves the svg image
#difficulty(int) difficulty level number
#colors(int) number of colors to choose from
#objects(int) number of objects to choose from
#rotation(int) max rotation possible
#skew(int) max skew possible
#scale(float) max scale possible
#x image width
#y image height
#returns numbers of objects and colors in picture
def image(difficulty, number_of_colors, number_of_objects, max_rotation, max_skew, max_scale, grid, overlap, mirror, group, x, y):

    #checks the parameters if using the server without web app, e.g with route only
    if (number_of_colors > 6 or number_of_colors < 1):
        raise ValueError("Number of colors must be in range(1, 6)")
    
    if (number_of_objects > 6 or number_of_objects < 1):
        raise ValueError("Number of objects must be in range(1, 6)")
    
    if (max_skew > 40 or max_skew < -40):
        raise ValueError("Skew must be in range(-40, 40)")

    if (grid != 0 and grid != 1):
        raise ValueError("Grid can be only 1(active) or 0(inactive)")

    if (overlap != 0 and overlap != 1):
        raise ValueError("Overlap can be only 1(active) or 0(inactive)")

    if (mirror != 0 and mirror != 1):
        raise ValueError("Mirroring can be only 1(active) or 0(inactive)")

    if (group != 0 and group != 1):
        raise ValueError("Grouping can be only 1(active) or 0(inactive)")

    if (grid == 1 and group == 1):
        raise RuntimeError("Grid and Grouping cant be 1(active) at the same time. Change one of them to 0(inactive)")

    all_shapes = []
    text_rect = []
    grid_coordinates = []
    counter = {}
    c = 0
    max_size = 105*max_scale
    q = 0
    length = 1

    #adjusting the count of all objects in image
    if (difficulty < 5):
        count = random.randint(1, 5)
    elif (difficulty < 9):
        count = random.randint(2, 7)
    elif (difficulty < 13):
        count = random.randint(3, 9)
    elif (difficulty < 17):
        count = random.randint(3, 11)
    elif (difficulty < 21):
        count = random.randint(4, 13)
    elif (difficulty < 25):
        count = random.randint(5, 15)
    elif (difficulty < 28):
        count = random.randint(5, 18)
    else:
        count = random.randint(9, 27)

    #choosing random colors and objects to use in image
    all_objects = ['circle', 'rectangle', 'ellipse', 'triangle', 'text', 'icon']
    all_colors = ['red', 'blue', 'green', 'black', 'orange', 'turquoise']
    objects = random.sample(all_objects, number_of_objects)
    if ('icon' in objects):
        all_colors.remove('black')
        colors = random.sample(all_colors, number_of_colors - 1)
        colors.append('black')
    else:
        colors = random.sample(all_colors, number_of_colors)
    group_objects = [3, 4, 5, 9]
    
    for d in range(0, number_of_colors):
        counter[colors[d]] = 0

    for i in range(0, number_of_objects):
        counter[objects[i]] = 0

    dwg = svgwrite.Drawing(filename='image.svg', debug=True)
    shapes = dwg.add(dwg.g(id='shapes'))

    #creates grid
    if (grid == 1):
        show_grid(dwg, max_size, x, y)

    #creates coordinates to randomly choose from that are in grid
    for p in range(1, int(x/max_size + 0.5)):
        for k in range(1, int(y/max_size + 0.5)):
            grid_coordinates.append((p*max_size, k*max_size))

    #adding objects to image until there is enough of them
    while (c < count and q != length):

        shape = random.choice(objects)
        color = random.choice(colors)

        #chooses random group shape, if it overlaps somewhere, chooses one object
        if (group == 1):
            group_count = random.choice(group_objects)
            group_coordinates = get_group_coordinates(group_count, max_size, x, y)
            length = len(group_coordinates)
            for f in range(0, length):
                var = dwg.circle(center=group_coordinates[f], r=(max_size/2))
                if overlap_control(var, all_shapes, overlap):
                    continue
                else:
                    length = 1
                    break

        #adding single object or group of objects to image
        for q in range(0, length):

            #randomizing every parameter
            scale = random.uniform(0.9*max_scale, max_scale)
            text_size = random.randint(30, 45)
            radius = random.randint(30, 50)
            radius_e = (random.randint(10, 20), random.randint(25, 35))
            size = random.randint(35, 45)
            size_r = (random.randint(30, 45), random.randint(30, 45))
            icon_size = random.randint(40, 45)

            rotation = random.randint(-max_rotation, max_rotation)
            icon_number = random.randint(1, 20)
            skew = random.randint(-max_skew, max_skew)

            #for every shape checking if it overlaps with anything, if not or if overlaping is on and it doesnt overlap too much, adds the shape to image
            if (shape == 'circle'):
                int_radius = int(radius*scale) + 1
                if (length == 1):
                    coordinates = get_coordinates(grid_coordinates, shape, 0, int_radius, grid, x, y)
                else:
                    coordinates = group_coordinates[q]
                circle = dwg.circle(center=coordinates, r=radius, fill=color)
                var = dwg.circle(center=coordinates, r=radius*scale)
                transform(circle, 0, scale, coordinates, 0, 0)
                if overlap_control(var, all_shapes, overlap):
                    shapes.add(circle)
                    all_shapes.append(var)
                    counter[color] = counter[color] + 1
                    counter[shape] = counter[shape] + 1
                    c = c + 1
                else:
                    continue
                
            elif (shape == 'rectangle'):
                radius = math.sqrt((size_r[0]/2)**2 + (size_r[1]/2)**2)
                final_radius = radius*1.5*scale
                int_radius = int(final_radius) + 1
                if (length == 1):
                    coordinates = get_coordinates(grid_coordinates, shape, size_r, int_radius, grid, x, y)
                else:
                    coordinates = group_coordinates[q]
                center = ((coordinates[0] + size_r[0]/2), (coordinates[1] + size_r[1]/2))
                rect = dwg.rect(insert=coordinates, size=size_r, fill=color)
                circle = dwg.circle(center=center, r=final_radius)
                transform(rect, skew, scale, center, rotation, 0)
                if overlap_control(circle, all_shapes, overlap):
                    shapes.add(rect)
                    all_shapes.append(circle)
                    counter[color] = counter[color] + 1
                    counter[shape] = counter[shape] + 1
                    c = c + 1
                else:
                    continue
                
            elif (shape == 'ellipse'):
                radius = max(radius_e)
                final_radius = radius*1.5*scale
                int_radius = int(final_radius)
                if (length == 1):
                    coordinates = get_coordinates(grid_coordinates, shape, 0, int_radius, grid, x, y)
                else:
                    coordinates = group_coordinates[q]
                ellipse = dwg.ellipse(center=coordinates, r=radius_e, fill=color)
                circle = dwg.circle(center=coordinates, r=final_radius)
                transform(ellipse, skew, scale, coordinates, rotation, 0)
                if overlap_control(circle, all_shapes, overlap):
                    shapes.add(ellipse)
                    all_shapes.append(circle)
                    counter[color] = counter[color] + 1
                    counter[shape] = counter[shape] + 1
                    c = c + 1
                else:
                    continue
                
            elif (shape == 'triangle'):
                height = math.sqrt(size**2 - (size/2)**2)
                radius = math.sqrt((size/2)**2 + (height/2)**2)
                final_radius = radius*1.5*scale
                int_radius = int(final_radius) + 1
                if (length == 1):
                    coordinates = get_coordinates(grid_coordinates, shape, height, int_radius, grid, x, y)
                else:
                    coordinates = group_coordinates[q]
                center = (coordinates[0], coordinates[1] + height/2)
                triangle = dwg.polygon(points=[coordinates, (coordinates[0] - size/2, coordinates[1] + height), (coordinates[0] + size/2, coordinates[1] + height)], fill=color)
                circle = dwg.circle(center=center, r=final_radius)
                transform(triangle, skew, scale, center, rotation, mirror)
                if overlap_control(circle, all_shapes, overlap):
                    shapes.add(triangle)
                    all_shapes.append(circle)
                    counter[color] = counter[color] + 1
                    counter[shape] = counter[shape] + 1
                    c = c + 1
                else:
                    continue
                
            elif (shape == 'text'):
                letter = random.choice(string.ascii_uppercase)
                radius = math.sqrt((text_size/2)**2 + (text_size/2)**2)
                final_radius = radius*1.5*scale
                int_radius = int(final_radius) + 1
                if (length == 1):
                    coordinates = get_coordinates(grid_coordinates, shape, (text_size, text_size), int_radius, grid, x, y)
                else:
                    coordinates = group_coordinates[q]
                center = ((coordinates[0] + (text_size/2)), (coordinates[1] - (text_size/2)))
                text = dwg.text(letter, insert=coordinates, fill=color, font_size=str(text_size) + 'px')
                circle = dwg.circle(center=center, r=final_radius)
                transform(text, skew, scale, center, rotation, mirror)
                if overlap_control(circle, all_shapes, overlap):
                    shapes.add(text)
                    all_shapes.append(circle)
                    counter[color] = counter[color] + 1
                    counter[shape] = counter[shape] + 1
                    c = c + 1
                else:
                    continue

            elif (shape == 'icon'):
                radius = math.sqrt(((icon_size/2)**2)*2)
                final_radius = radius*1.5*scale
                int_radius = int(final_radius) + 1
                if (length == 1):
                    coordinates = get_coordinates(grid_coordinates, shape, icon_size, int_radius, grid, x, y)
                else:
                    coordinates = group_coordinates[q]
                center = ((coordinates[0] + icon_size/2), (coordinates[1] + icon_size/2))
                icon = dwg.image('Icons/Icon (' + str(icon_number) + ').svg', insert=coordinates, size=(icon_size, icon_size))
                circle = dwg.circle(center=center, r=final_radius)
                transform(icon, skew, scale, center, rotation, mirror)
                if overlap_control(circle, all_shapes, overlap):
                    shapes.add(icon)
                    all_shapes.append(circle)
                    counter['black'] = counter['black'] + 1
                    counter[shape] = counter[shape] + 1
                    c = c + 1
                else:
                    continue
            else:
                break
        counter['all'] = c
    dwg.save()
    if (c <= 6 or group == 1):
        return {'all': c}
    else:
        return counter

#shows the grid (draws it)
#dwg is the image reference
#max_size is the max size of single object in diameter
#x image width
#y image height
def show_grid(dwg, max_size, x, y):
    grid_lines = dwg.add(dwg.g(id='lines', stroke='red'))
    for p in range(1, int(x/max_size + 0.5) + 1):
        grid_lines.add(dwg.line(start=(p*max_size - 0.5*max_size, 0), end=(p*max_size - 0.5*max_size, y)))
    for k in range(1, int(y/max_size + 0.5) + 1):
        grid_lines.add(dwg.line(start=(0, k*max_size - 0.5*max_size), end=(x, k*max_size - 0.5*max_size)))

#coordinates are possible grid coordinates generated at the start of image function
#shape is object shape
#size is object size
#int_radius is object radius
#grid signals if the grid is on or not
#x image width
#y image height
#returns random coordinates for given object (single tuple)
def get_coordinates(coordinates, shape, size, int_radius, grid, x, y):
    if (grid == 0):
        return (random.randint(2*int_radius, x - 2*int_radius), random.randint(2*int_radius, y - 2*int_radius))
    else:
        coordinate = random.choice(coordinates)
        if (shape == 'rectangle'):
            return ((coordinate[0] - size[0]/2), (coordinate[1] - size[1]/2))
        elif (shape == 'triangle'):
            return (coordinate[0], (coordinate[1] - size/2))
        elif (shape == 'text'):
            return ((coordinate[0] - size[0]/2), (coordinate[1] + size[1]/2))
        elif (shape == 'icon'):
            return ((coordinate[0] - size/2), (coordinate[1] - size/2))
        else:
            return (coordinate)

#group is the number of objects in group
#size is the max object size in diameter
#x image width
#y image height
#returns random set of coordinates for groups of objects (multiple tuples)
def get_group_coordinates(group, size, x, y):
    coordinates = []
    start = random.randint(int(1.5*size), x - int(1.5*size)), random.randint(int(1.5*size), y - int(1.5*size))
    coordinates.append(start)

    height = math.sqrt(size**2 - (size/2)**2)
    
    if (group == 3):
        coordinates.append((start[0] + size, start[1]))
        coordinates.append((start[0] + size/2, start[1] + height))
        return (coordinates)
    elif (group == 4):
        coordinates.append((start[0] + size, start[1]))
        coordinates.append((start[0], start[1] + size))
        coordinates.append((start[0] + size, start[1] + size))
        return (coordinates)
    elif (group == 5):
        coordinates.append((start[0] + size, start[1]))
        coordinates.append((start[0] - size, start[1]))
        coordinates.append((start[0], start[1] + size))
        coordinates.append((start[0], start[1] - size))
        return (coordinates)
    elif (group == 9):
        coordinates.append((start[0] + size, start[1]))
        coordinates.append((start[0] - size, start[1]))
        coordinates.append((start[0], start[1] + size))
        coordinates.append((start[0], start[1] - size))
        coordinates.append((start[0] + size, start[1] + size))
        coordinates.append((start[0] - size, start[1] - size))
        coordinates.append((start[0] - size, start[1] + size))
        coordinates.append((start[0] + size, start[1] - size))
        return (coordinates)

#transforms object then moves it back to its position
#object is reference to object
#skew is the skew value
#scale is the scale value
#center is the center of the object
#rotation is the rotation value
#mirror signals if the mirroring is on or not
def transform(object, skew, scale, center, rotation, mirror):
    mixins.Transform.rotate(object, rotation, center)
    mixins.Transform.translate(object, center)

    #randomly chooses horizontal or vertical skew
    if (random.getrandbits(1)):
        mixins.Transform.skewX(object, skew)
    else:
        mixins.Transform.skewY(object, skew)

    #mirrors the object horizontaly or verticaly
    if (mirror == 1):
        if (random.getrandbits(1)):
            mixins.Transform.scale(object, (scale, -scale))
        else:
            mixins.Transform.scale(object, (-scale, scale))
    else:
        mixins.Transform.scale(object, scale)

    #returns the object to its starting position
    mixins.Transform.translate(object, (-center[0], -center[1]))

#checks if object is overlaping with any other objects already in image, if overlap is on allows 25% overlaping of objects  
def overlap_control(circle, all_shapes, overlap):
    if (overlap == 0):
        for j in range (0, len(all_shapes)):
            if math.sqrt((circle['cx'] - all_shapes[j]['cx'])**2 + (circle['cy'] - all_shapes[j]['cy'])**2) < circle['r'] + all_shapes[j]['r']:
                return False
    else:
        for j in range (0, len(all_shapes)):
            if math.sqrt((circle['cx'] - all_shapes[j]['cx'])**2 + (circle['cy'] - all_shapes[j]['cy'])**2) < (circle['r'] + all_shapes[j]['r'])/1.5:
                return False
    return True
