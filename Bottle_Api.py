from bottle import route, request, run, template, static_file, hook, response
from Pictures_SVG import image
import json, argparse

#enables CORS
@hook('after_request')
def enable_cors():
    response.headers['Access-Control-Allow-Origin'] = '*'
    response.headers['Access-Control-Allow-Methods'] = 'PUT, GET, POST, DELETE, OPTIONS'
    response.headers['Content-Type'] = 'application/json'
    response.headers['Access-Control-Allow-Headers'] = 'Origin, Content-Type, X-Auth-Token'

#basic route which creates the image based on the parameters(if any parameter is missing, uses default value)
#returns dictionary with numbers of objects and colors in image
#route example: http://localhost:8081/api?diff=10&colors=6&objects=5&rotation=55&skew=30&scale=1.25&grid=0&overlap=0&mirror=0&group=1&x=1920&y=974
@route('/api', methods=['POST', 'GET'])
def get_params():
    objects = {}
    if (request.params.get('diff') is None):
        difficulty = 1
    else:
        difficulty = int(request.params.get('diff'))
    if (request.params.get('colors') is None):
        number_of_colors = 1
    else:  
        number_of_colors = int(request.params.get('colors'))
    if (request.params.get('objects') is None):
        number_of_objects = 1
    else:
        number_of_objects = int(request.params.get('objects'))
    if (request.params.get('rotation') is None):
        max_rotation = 1
    else:
        max_rotation = float(request.params.get('rotation'))
    if (request.params.get('skew') is None):
        max_skew = 1
    else:
        max_skew = float(request.params.get('skew'))
    if (request.params.get('scale') is None):
        max_scale = 1
    else:
        max_scale = float(request.params.get('scale'))
    if (request.params.get('grid') is None):
        grid = 0
    else:
        grid = int(request.params.get('grid'))
    if (request.params.get('overlap') is None):
        overlap = 0
    else:
        overlap = int(request.params.get('overlap'))
    if (request.params.get('mirror') is None):
        mirror = 0
    else:
        mirror = int(request.params.get('mirror'))
    if (request.params.get('group') is None):
        group = 0
    else:
        group = int(request.params.get('group'))
    if (request.params.get('x') is None):
        x = 1920
    else:
        x = int(request.params.get('x'))
    if (request.params.get('y') is None):
        y = 974
    else:
        y = int(request.params.get('y'))
    objects = image(difficulty, number_of_colors, number_of_objects, max_rotation, max_skew, max_scale, grid, overlap, mirror, group, x, y)
    return json.dumps(objects)
    #can be used to show image after calling the route with parameters, only for testing
    #return static_file('testerino.svg', root='./')

#serves the icon images
@route('/Icons/<filename>')
def server_static(filename):
    return static_file(filename, root='./Icons')

#the host and port to run the app
run(host='localhost', port=8081, debug=True)
#can be used to run the server on your own IP adress, only for testing
#run(host='0.0.0.0', port=80)
