First I would like to thank you for trying out my subitizing game. I hope you will like it and learn to subitize better.
If you are interested in this project, you need to download it as zip or tar and then unpack it. 
To play the game you will only need the actual version of Python on your computer. All other dependencies are in the project folder.
How to start the game:
1.You need to start the server for image generation. Which is Bottle_Api.py. You can open and run it for example with Python IDLE.
2.Now you need to run the game itself, which would be the Index.html file. I would recommend using Google Chrome.
3.Check instructions and when ready just Start the game. Good luck!

With any problems, contact me at kotrcb@gmail.com or 433718@mail.muni.cz 